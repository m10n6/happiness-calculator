<div class="wrap">
	<h2>Happiness Score Logs</h2>
	<div class="clearfix"></div>
	<hr/>
	<div class="clearfix"></div>
	<div class="row">
		<div class="col-md-12">
			<!-- -->
			<table id="tblLogs" class="table table-striped" >
				<thead>
					<tr>
						<th rowspan="2">Email</th>
						<th rowspan="2">UID</th>
						<th colspan="2">Family</th>
						<th colspan="2">Friends</th>
						<th colspan="2">Finance</th>
						<th colspan="2">Health</th>
						<th colspan="2">Career</th>
						<th colspan="2">Personal<br>Growth</th>
						<th colspan="2">Spirituality/<br>Contribution</th>
						<th rowspan="2">Total</th>
					</tr>
					<tr>
						<th >1st</th>
						<th >2nd</th>
						<th >1st</th>
						<th >2nd</th>
						<th >1st</th>
						<th >2nd</th>
						<th >1st</th>
						<th >2nd</th>
						<th >1st</th>
						<th >2nd</th>
						<th >1st</th>
						<th >2nd</th>
						<th >1st</th>
						<th >2nd</th>
					</tr>
				</thead>
				<tbody>
			<?php
				global $wpdb, $table_prefix;
				$sql_log = "select * from `".$table_prefix."happiness_score` order by `dateadded` desc";
				$rs_logs  = $wpdb->get_results($sql_log);
				if(count($rs_logs)>0){
					foreach($rs_logs as $result){
						// echo '<option value="'.$zones->abbrv.'">'.$zones->zone.' - ['.$zones->abbrv.']</option>';
						// `email` text NOT NULL,
	          // `family1` text NOT NULL,
	          // `family2` text NOT NULL,
	          // `friends1` text NOT NULL,
	          // `friends2` text NOT NULL,
	          // `finance1` text NOT NULL,
	          // `finance2` text NOT NULL,
	          // `health1` text NOT NULL,
	          // `health2` text NOT NULL,
	          // `career1` text NOT NULL,
	          // `career2` text NOT NULL,
	          // `growth1` text NOT NULL,
	          // `growth2` text NOT NULL,
	          // `spirit1` text NOT NULL,
	          // `spirit2` text NOT NULL,
	          // `totalresult` text NOT NULL,
						echo '
						<tr>
							<td >'.$result->email.'</td>
							<td >'.$result->user_id.'</td>

							<td >'.$result->family1.'</td>
							<td >'.$result->family2.'</td>

							<td >'.$result->friends1.'</td>
							<td >'.$result->friends2.'</td>

							<td >'.$result->finance1.'</td>
							<td >'.$result->finance2.'</td>

							<td >'.$result->health1.'</td>
							<td >'.$result->health2.'</td>

							<td >'.$result->career1.'</td>
							<td >'.$result->career2.'</td>

							<td >'.$result->growth1.'</td>
							<td >'.$result->growth2.'</td>

							<td >'.$result->spirit1.'</td>
							<td >'.$result->spirit2.'</td>

							<td >'.$result->totalresult.'</td>
						</tr>
						';
					}
				}
			?>

		</tbody>
			</table>
			<!-- -->
		</div>
	</div>


</div>

<script>
jQuery.noConflict();
(function( $ ) {

	$(document).ready(function(){
			$('#tblLogs').DataTable({
				"ordering": false,
				"scrollX": true
			});
			// $('#tblLogs').css({
			// 	'width' : '100% !important'
			// });
	});

})(jQuery);
</script>
