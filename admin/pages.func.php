<?php

add_action( 'admin_init', 'happinessScore_plugin_admin_init' );
function happinessScore_plugin_admin_init() {

    wp_register_script('happinessScore_js1', HC_PLUGINPATH.'lib/bootstrap/js/bootstrap.min.js');
    wp_register_script('happinessScore_js2', HC_PLUGINPATH.'lib/datatables/jquery.dataTables.min.js');
    wp_register_script('happinessScore_js3', HC_PLUGINPATH.'lib/dropzone/dropzone.js');
    wp_register_script('happinessScore_js4', HC_PLUGINPATH.'lib/jquery.growl/javascripts/jquery.growl.js');
    wp_register_script('happinessScore_js5', HC_PLUGINPATH.'lib/fileupload/filedrag.js');
    wp_register_script('happinessScore_js6', HC_PLUGINPATH.'lib/ckeditor/ckeditor.js');

    wp_register_script('happinessScore_js7', HC_PLUGINPATH.'lib/flot/jquery.flot.js');
    wp_register_script('happinessScore_js8', HC_PLUGINPATH.'lib/flot/jquery.flot.time.js');
    wp_register_script('happinessScore_js9', HC_PLUGINPATH.'lib/flot/jquery.flot.selection.js');
    wp_register_script('happinessScore_js10', HC_PLUGINPATH.'lib/select2/dist/js/select2.min.js');



    wp_register_style('happinessScore_st1', HC_PLUGINPATH.'lib/bootstrap/css/bootstrap.min.css');
    wp_register_style('happinessScore_st2', HC_PLUGINPATH.'lib/font-awesome/css/font-awesome.min.css');
    wp_register_style('happinessScore_st3', HC_PLUGINPATH.'lib/datatables/jquery.dataTables.min.css');
    wp_register_style('happinessScore_st4', HC_PLUGINPATH.'lib/dropzone/dropzone.css');
    wp_register_style('happinessScore_st5', HC_PLUGINPATH.'lib/jquery.growl/stylesheets/jquery.growl.css');

    wp_register_style('happinessScore_st6', HC_PLUGINPATH.'lib/flot/examples/examples.css');
    wp_register_style('happinessScore_st7', HC_PLUGINPATH.'lib/select2/dist/css/select2.min.css');


}

add_action( 'admin_menu', 'register_happinessScore_admin_page' );

function register_happinessScore_admin_page(){

    /* submenu */

    // add_submenu_page( string $parent_slug, string $page_title, string $menu_title, string $capability, string $menu_slug, callable $function = '' );
    $c_id = get_current_user_id();
    $user = new WP_User($c_id);
    $u_role =  $user->roles[0];
    // print_r($user);
    switch($u_role){
        // case 'editor':
        //     add_menu_page( 'CampDraft', 'CampDraft', 'moderate_comments', 'cdhomepage', 'happinessScore_home_page', plugin_dir_url( '/').'campdraft-plugin/images/cdicon.png', 6 );
        //     add_submenu_page( 'cdhomepage', 'Committees', 'Committees', 'moderate_comments', 'cdp-committees', 'happinessScore_committees_page' );
        // break;
        // case 'committee':
        //     add_menu_page( 'CampDraft', 'CampDraft', 'moderate_comments', 'cdhomepage', 'happinessScore_home_page', plugin_dir_url( '/').'campdraft-plugin/images/cdicon.png', 6 );
        //     add_submenu_page( 'cdhomepage', 'Committees', 'Committees', 'moderate_comments', 'cdp-committees', 'happinessScore_committees_page' );
        // break;

        // case 'subscriber':
        //     add_menu_page( 'CampDraft', 'CampDraft', 'read', 'cdhomepage', 'happinessScore_home_page', plugin_dir_url( '/').'campdraft-plugin/images/cdicon.png', 6 );
        //     add_submenu_page( 'cdhomepage', 'Members List', 'Members List', 'read', 'cdp-memberlist', 'happinessScore_memberlist_page' );
        // break;

        // case 'members':
        //     add_menu_page( 'CampDraft', 'CampDraft', 'read', 'cdhomepage', 'happinessScore_home_page', plugin_dir_url( '/').'campdraft-plugin/images/cdicon.png', 6 );
        //     add_submenu_page( 'cdhomepage', 'Members List', 'Members List', 'read', 'cdp-memberlist', 'happinessScore_memberlist_page' );
        // break;

        default:
            // add_menu_page( 'Happiness Score', 'Happiness Score', 'manage_options', 'hchomepage', 'happiness_home_page', plugin_dir_url( '/').'happiness-calculator/lib/images/admin-icon20x20.png', 6 );
            // add_submenu_page( 'hchomepage', 'Dashboard', 'Dashboard', 'manage_options', 'hchomepage', 'happiness_home_page' );
            // add_submenu_page( 'hchomepage', 'Users', 'Users', 'manage_options', 'hc-admin-users', 'happiness_users_page' );
            // add_submenu_page( 'hchomepage', 'Logs', 'Logs', 'manage_options', 'hc-admin-logs', 'happiness_logs_page' );
            add_menu_page( 'Happiness Score', 'Happiness Score', 'manage_options', 'hchomepage', 'happiness_logs_page', plugin_dir_url( '/').'happiness-calculator/lib/images/admin-icon20x20.png', 6 );
            add_submenu_page( 'hchomepage', 'Logs', 'Logs', 'manage_options', 'hchomepage', 'happiness_logs_page' );
                // add_submenu_page( 'cdp-firstaid', 'First Aid1', 'First Aid1', 'manage_options', 'cdp-firstaid1', 'happinessScore_firstaid_page' );
        break;

    }


}

function happiness_home_page(){
    wp_enqueue_script( 'happinessScore_js1', HC_PLUGINPATH.'lib/bootstrap/js/bootstrap.min.js', array(), '1.0.0', true );
    wp_enqueue_script( 'happinessScore_js2', HC_PLUGINPATH.'lib/datatables/jquery.dataTables.min.js', array(), '1.0.0', true );

    wp_enqueue_script( 'happinessScore_js7', HC_PLUGINPATH.'lib/flot/jquery.flot.js', array(), '1.0.0', true );
    wp_enqueue_script( 'happinessScore_js8', HC_PLUGINPATH.'lib/flot/jquery.flot.time.js', array(), '1.0.0', true );
    wp_enqueue_script( 'happinessScore_js9', HC_PLUGINPATH.'lib/flot/jquery.flot.selection.js', array(), '1.0.0', true );


    wp_enqueue_style( 'happinessScore_st1');
    wp_enqueue_style( 'happinessScore_st2');
    wp_enqueue_style( 'happinessScore_st3');

    wp_enqueue_style( 'happinessScore_st6');

    include_once('dashboard.page.php');
}

function happiness_users_page(){
    wp_enqueue_script( 'happinessScore_js1', HC_PLUGINPATH.'lib/bootstrap/js/bootstrap.min.js', array(), '1.0.0', true );
    wp_enqueue_script( 'happinessScore_js2', HC_PLUGINPATH.'lib/datatables/jquery.dataTables.min.js', array(), '1.0.0', true );

    wp_enqueue_script( 'happinessScore_js7', HC_PLUGINPATH.'lib/flot/jquery.flot.js', array(), '1.0.0', true );
    wp_enqueue_script( 'happinessScore_js8', HC_PLUGINPATH.'lib/flot/jquery.flot.time.js', array(), '1.0.0', true );
    wp_enqueue_script( 'happinessScore_js9', HC_PLUGINPATH.'lib/flot/jquery.flot.selection.js', array(), '1.0.0', true );


    wp_enqueue_style( 'happinessScore_st1');
    wp_enqueue_style( 'happinessScore_st2');
    wp_enqueue_style( 'happinessScore_st3');

    wp_enqueue_style( 'happinessScore_st6');

    include_once('users.page.php');
}

function happiness_logs_page(){
    wp_enqueue_script( 'happinessScore_js1', HC_PLUGINPATH.'lib/bootstrap/js/bootstrap.min.js', array(), '1.0.0', true );
    wp_enqueue_script( 'happinessScore_js2', HC_PLUGINPATH.'lib/datatables/jquery.dataTables.min.js', array(), '1.0.0', true );

    // wp_enqueue_script( 'happinessScore_js7', HC_PLUGINPATH.'lib/flot/jquery.flot.js', array(), '1.0.0', true );
    // wp_enqueue_script( 'happinessScore_js8', HC_PLUGINPATH.'lib/flot/jquery.flot.time.js', array(), '1.0.0', true );
    // wp_enqueue_script( 'happinessScore_js9', HC_PLUGINPATH.'lib/flot/jquery.flot.selection.js', array(), '1.0.0', true );


    wp_enqueue_style( 'happinessScore_st1');
    wp_enqueue_style( 'happinessScore_st2');
    wp_enqueue_style( 'happinessScore_st3');

    // wp_enqueue_style( 'happinessScore_st6');

    include_once('logs.page.php');
}
