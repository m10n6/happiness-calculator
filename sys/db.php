<?php
/*
	DATABASE 
Family
Friends
Finance
Health
Career
Personal Growth
Spirituality/Contribution

*/

if (!class_exists('happiness_db_class')) {
class happiness_db_class {
    function createDB() {
        global $wpdb, $table_prefix;
        $sql1 = "CREATE TABLE IF NOT EXISTS `".$table_prefix."happiness_score` (
          `hid` int(10) NOT NULL auto_increment,
          `user_id` text NOT NULL,
          `email` text NOT NULL,
          `family1` text NOT NULL,
          `family2` text NOT NULL,
          `friends1` text NOT NULL,
          `friends2` text NOT NULL,
          `finance1` text NOT NULL,
          `finance2` text NOT NULL,
          `health1` text NOT NULL,
          `health2` text NOT NULL,
          `career1` text NOT NULL,
          `career2` text NOT NULL,
          `growth1` text NOT NULL,
          `growth2` text NOT NULL,
          `spirit1` text NOT NULL,
          `spirit2` text NOT NULL,
		  `totalresult` text NOT NULL,
          `dateadded` DATETIME,
          PRIMARY KEY  (`hid`),
          UNIQUE KEY `hid` (`hid`)
        ) ENGINE=MyISAM  DEFAULT CHARSET=UTF8 AUTO_INCREMENT=1 ;";
            
        // Execute query
        $wpdb->query($sql1);

    }

    function upgradeDB(){
        global $wpdb, $table_prefix;
        // $wpdb->query("ALTER TABLE `".$table_prefix."campdraft_titles` ADD `rider_status` TEXT NULL AFTER `logo`");
        // $wpdb->query("ALTER TABLE `".$table_prefix."campdraft_titles` ADD `horse_status` TEXT NULL AFTER `rider_status`");

        // $wpdb->query("ALTER TABLE `".$table_prefix."campdraft_adminevent` ADD `rider_restrict` TEXT NULL AFTER `rider_class`");


        // $col = $wpdb->query("select `level` from `".$table_prefix."cssau_trans`");
        // if (!$col){
            // $wpdb->query("ALTER TABLE `".$table_prefix."cssau_trans` ADD `level` TEXT NOT NULL AFTER `extra`");
        // }
        
        // $col = $wpdb->query("select `level` from `".$table_prefix."cssau_trans`");
        // if (!$col){
            // $wpdb->query("ALTER TABLE `".$table_prefix."cssau_trans` ADD `currency` TEXT NOT NULL AFTER `price`");
        // }

        // $col = $wpdb->query("select `level` from `".$table_prefix."cssau_event`");
        // if (!$col){
            // $wpdb->query("ALTER TABLE `".$table_prefix."cssau_event` ADD `currency` TEXT NOT NULL AFTER `price`");
        // }
    }
}
}

$mlrHappinessDB = new happiness_db_class();
$mlrHappinessDB->upgradeDB();
$mlrHappinessDB->createDB();