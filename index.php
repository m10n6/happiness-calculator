<?php
/*
Plugin Name: Happiness Calculator
Plugin URI: http://markramosonline.com/wp-plugins/happiness-calculator
Description: Happiness Calculator, is custom built for Mike Turton. <strong>Usage:</strong> <code>[happy-calc]</code>
Version: 1.0
Author: Mark Lyndon Ramos
Author URI: http://markramosonline.com
License: A "Slug" license name e.g. GPL2
*/
/*  Copyright 2018  Mark Lyndon Ramos  (email : info@markramosonline.com)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License, version 2, as
    published by the Free Software Foundation.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

define('HC_PLUGINPATH', plugin_dir_url( '/') . 'happiness-calculator/');
if(is_admin()){
    include_once('sys/db.php');
    include_once('admin/admin.php');
}


function HappyCalculator( $atts ){

    // wp_register_script('happiness-calc-js1', HC_PLUGINPATH.'lib/bootstrap/js/bootstrap.min.js');
    // wp_register_script('happiness-calc-js2', HC_PLUGINPATH.'js/happy-script.js');
    wp_register_script('happiness-calc-js3', HC_PLUGINPATH.'lib/sweetalert.min.js');
    wp_register_script('happiness-calc-js4', HC_PLUGINPATH.'lib/jquery.growl/javascripts/jquery.growl.js');
    // wp_register_script('campdraft_js2', HC_PLUGINPATH.'lib/datatables/jquery.dataTables.min.js');
    // wp_register_script('campdraft_js3', HC_PLUGINPATH.'lib/bootstrap-datepicker/js/bootstrap-datepicker.js');
    // wp_register_script('campdraft_js4', HC_PLUGINPATH.'js/script.js');

    wp_enqueue_script( 'happiness-calc-js1', HC_PLUGINPATH.'lib/bootstrap/js/bootstrap.min.js', array(), '1.0.0', true );
    // wp_enqueue_script( 'happiness-calc-js2', HC_PLUGINPATH.'js/happy-script.js', array(), '1.0.0', true );
    wp_enqueue_script( 'happiness-calc-js3', HC_PLUGINPATH.'lib/sweetalert.min.js', array(), '1.0.0', true );
    wp_enqueue_script( 'happiness-calc-js4', HC_PLUGINPATH.'lib/jquery.growl/javascripts/jquery.growl.js', array(), '1.0.0', true );
    // wp_enqueue_script( 'campdraft_js2', HC_PLUGINPATH.'lib/datatables/jquery.dataTables.min.js', array(), '1.0.0', true );
    // wp_enqueue_script( 'campdraft_js3', HC_PLUGINPATH.'lib/bootstrap-datepicker/js/bootstrap-datepicker.js', array(), '0.0.1', true );
    // wp_enqueue_script( 'jquery-ui-datepicker' );
    // wp_enqueue_script( 'campdraft_js4', HC_PLUGINPATH.'js/script.js', array(), '0.0.1', true );


    wp_register_style('happiness-calc-st1', HC_PLUGINPATH.'lib/bootstrap/css/bootstrap.min.css');
    wp_register_style('happiness-calc-st2', HC_PLUGINPATH.'lib/font-awesome/css/font-awesome.min.css');
    wp_register_style('happiness-calc-st3', HC_PLUGINPATH.'css/happiness.css');
    wp_register_style('happiness-calc-st4', HC_PLUGINPATH.'lib/jquery.growl/stylesheets/jquery.growl.css');
    // wp_register_style('mlrcd_st3', HC_PLUGINPATH.'lib/datatables/jquery.dataTables.min.css');
    // wp_register_style('mlrcd_st4', HC_PLUGINPATH.'lib/bootstrap-datepicker/css/bootstrap-datepicker.css');

    wp_enqueue_style('happiness-calc-st1');
    wp_enqueue_style('happiness-calc-st2');
    wp_enqueue_style('happiness-calc-st3');
    wp_enqueue_style('happiness-calc-st4');

    if(!is_user_logged_in()){
        wp_register_script('happiness-calc-js2', HC_PLUGINPATH.'js/happy-script.js');
        wp_enqueue_script( 'happiness-calc-js2', HC_PLUGINPATH.'js/happy-script.js', array(), '1.0.0-'.date('YmdHis'), true );
    }else{
        wp_register_script('happiness-calc-js2', HC_PLUGINPATH.'js/happy-script-logged.js');
        wp_enqueue_script( 'happiness-calc-js2', HC_PLUGINPATH.'js/happy-script-logged.js', array(), '1.0.0-'.date('YmdHis'), true );

		    /**
		     * @example Safe usage:
		     * $current_user = wp_get_current_user();
		     * if ( ! $current_user->exists() ) {
		     *     return;
		     * }
		     */
        $current_user = wp_get_current_user();
        $user_values = '
          <script>
            var hs_uname = \''.$current_user->user_login.'\';
            var hs_email = \''.$current_user->user_email.'\';
            var hs_id = \''.$current_user->ID.'\';
            var hs_fname = \''.$current_user->user_firstname.'\';
            var hs_lname = \''.$current_user->user_lastname.'\';
            var hs_dname = \''.$current_user->display_name.'\';
          </script>
        ';
		    // echo '<strong>Username: </strong>' . $current_user->user_login . '<br />';
		    // echo 'User email: ' . $current_user->user_email . '<br />';
		    // echo 'User first name: ' . $current_user->user_firstname . '<br />';
		    // echo 'User last name: ' . $current_user->user_lastname . '<br />';
		    // echo 'User display name: ' . $current_user->display_name . '<br />';
		    // echo 'User ID: ' . $current_user->ID . '<br />';
    }

    // wp_enqueue_style('mlrcd_st3');
    // wp_enqueue_style('mlrcd_st4');
                    // Family
                    // Friends
                    // Finance
                    // Health
                    // Career
                    // Personal Growth
                    // Spirituality/Contribution
    $html_ = '
        <div class="container">
            <div class="col-md-12">

                <div class="row">
                    <div class="col-md-3">
                        <div class="row">
                            <div class="col-md-12" id="label_column">
                                <img src="'.HC_PLUGINPATH.'lib/images/calc-logo.png" class="img-responsive center-block">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 text-right">
                                <h4>Family</h4>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 text-right">
                                <h4>Friends</h4>
                                <div class="myspacer20"></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 text-right">
                                <h4>Finance</h4>
                                <div class="myspacer20"></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 text-right">
                                <h4>Health</h4>
                                <div class="myspacer20"></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 text-right">
                                <h4>Career</h4>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 text-right">
                                <h4>Personal Growth</h4>
                                <div class="myspacer20"></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 text-right">
                                <h4>Spirituality/Contribution</h4>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="row">
                            <div class="col-md-12 text-center" id="first_column">
                                <h3>first</h3>
                                <p>rate the importance of each category</p>
                                <ul>
                                    <li>enter numbers 1 to 7</li>
                                    <li>1 is most important</li>
                                    <li>7 is least important</li>
                                    <li>each number can only be used once</li>
                                </ul>
                            </div>
                        </div>
                        <div class="row input-row">
                            <div class="col-md-12">
								<h4 class="mobile-header-title first-mobile-title">Family</h4>
                                <input type="number" class="form-control text-center input-imp" id="impFamily" tabindex="1">
                            </div>
                        </div>
                        <div class="row input-row">
                            <div class="col-md-12">
								<h4 class="mobile-header-title first-mobile-title">Friends</h4>
                                <input type="number" class="form-control text-center input-imp" id="impFriends" tabindex="2">
                                <div class="myspacer20"></div>
                            </div>
                        </div>
                        <div class="row input-row">
                            <div class="col-md-12">
								<h4 class="mobile-header-title first-mobile-title">Finance</h4>
                                <input type="number" class="form-control text-center input-imp" id="impFinance" tabindex="3">
                                <div class="myspacer20"></div>
                            </div>
                        </div>
                        <div class="row input-row">
                            <div class="col-md-12">
								<h4 class="mobile-header-title first-mobile-title">Health</h4>
                                <input type="number" class="form-control text-center input-imp" id="impHealth" tabindex="4">
                                <div class="myspacer20"></div>
                            </div>
                        </div>
                        <div class="row input-row">
                            <div class="col-md-12">
								<h4 class="mobile-header-title first-mobile-title">Career</h4>
                                <input type="number" class="form-control text-center input-imp" id="impCareer" tabindex="5">
                                <div class="myspacer20"></div>
                            </div>
                        </div>
                        <div class="row input-row">
                            <div class="col-md-12">
								<h4 class="mobile-header-title first-mobile-title">Personal Growth</h4>
                                <input type="number" class="form-control text-center input-imp" id="impPersonal" tabindex="6">
                                <div class="myspacer20"></div>
                            </div>
                        </div>
                        <div class="row input-row">
                            <div class="col-md-12">
								<h4 class="mobile-header-title first-mobile-title">Spirituality/Contribution</h4>
                                <input type="number" class="form-control text-center input-imp" id="impContribution" tabindex="7">
                                <div class="myspacer20"></div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="row">
                            <div class="col-md-12 text-center" id="second_column">
                                <h3>second</h3>
                                <p>score your happiness level for each category</p>
                                <ul>
                                    <li>enter numbers 1 to 10</li>
                                    <li>1 is lowest</li>
                                    <li>10 is highest</li>
                                    <li>you can repeat any number</li>
                                </ul>
                            </div>
                        </div>
                        <div class="row input-row">
                            <div class="col-md-12">
								<h4 class="mobile-header-title second-mobile-title">Family</h4>
                                <input type="number" class="form-control text-center input-wer" id="werFamily" tabindex="8">
                                <div class="myspacer20"></div>
                            </div>
                        </div>
                        <div class="row input-row">
                            <div class="col-md-12">
								<h4 class="mobile-header-title second-mobile-title">Friends</h4>
                                <input type="number" class="form-control text-center input-wer" id="werFriends" tabindex="9">
                                <div class="myspacer20"></div>
                            </div>
                        </div>
                        <div class="row input-row">
                            <div class="col-md-12">
								<h4 class="mobile-header-title second-mobile-title">Finance</h4>
                                <input type="number" class="form-control text-center input-wer" id="werFinance" tabindex="10">
                                <div class="myspacer20"></div>
                            </div>
                        </div>
                        <div class="row input-row">
                            <div class="col-md-12">
								<h4 class="mobile-header-title second-mobile-title">Health</h4>
                                <input type="number" class="form-control text-center input-wer" id="werHealth" tabindex="11">
                                <div class="myspacer20"></div>
                            </div>
                        </div>
                        <div class="row input-row">
                            <div class="col-md-12">
								<h4 class="mobile-header-title second-mobile-title">Career</h4>
                                <input type="number" class="form-control text-center input-wer" id="werCareer" tabindex="12">
                                <div class="myspacer20"></div>
                            </div>
                        </div>
                        <div class="row input-row">
                            <div class="col-md-12">
								<h4 class="mobile-header-title second-mobile-title">Personal Growth</h4>
                                <input type="number" class="form-control text-center input-wer" id="werPersonal" tabindex="13">
                                <div class="myspacer20"></div>
                            </div>
                        </div>
                        <div class="row input-row">
                            <div class="col-md-12">
								<h4 class="mobile-header-title second-mobile-title">Spirituality/Contribution</h4>
                                <input type="number" class="form-control text-center input-wer" id="werContribution" tabindex="14">
                                <div class="myspacer20"></div>
                            </div>
                        </div>
                    </div>


                    <div class="col-md-3">
                        <div class="row">
                            <div class="col-md-12">

                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <input type="hidden" class="form-control text-center " id="resFamily" >
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <input type="hidden" class="form-control text-center " id="resFriends" >
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <input type="hidden" class="form-control text-center " id="resFinance" >
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <input type="hidden" class="form-control text-center " id="resHealth" >
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <input type="hidden" class="form-control text-center " id="resCareer" >
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <input type="hidden" class="form-control text-center " id="resPersonal" >
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <input type="hidden" class="form-control text-center " id="resContribution" >
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-3 text-center col-remove">
                        <div style="clear:both; width: 100%; height: 50px;"></div>
                        <div id="happiness_result">
                          <span style="font-size: 26px; font-weight: bold;">Placeholder Title</span>
                          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras volutpat dolor a consectetur dapibus. </p>
                        </div>
                        <!--<h4 id="happiness_result"></h4>-->
                    </div>
                    <div class="col-md-6 text-center">
                        <button type="button" class="btn btn-primary" id="btnSubmitAns"> <i class="fa fa-smile-o" aria-hidden="true"></i> Get my Happiness Score</button>
                        <!--
                        <a style=\' font-size: 1.5em;text-transform: uppercase;text-decoration: none;font-family: Arial, Helvetica, sans-serif;padding: .3em .6em;letter-spacing: .05em;color: #333;\' href=\'javascript://\' data-opf-trigger=\'p2c186951f3\'>Please send my Happiness Score</a><script type=\'text/javascript\' async=\'true\' src=\'https://app.ontraport.com/js/ontraport/opt_assets/drivers/opf.js\' data-opf-uid=\'p2c186951f3\' data-opf-params=\'borderColor=#ffffff&borderSize=0px&formHeight=397&formWidth=480px&popPosition=mc&instance=n658936840\'></script>
                        -->
                    </div>
                    <div class="col-md-3 text-center"></div>
                </div>

            </div>
        </div>



<!-- Modal -->
<div id="myHappinessModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">We will email you your score</h4>
      </div>
      <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label>First Name</label>
                    <input type="text" class="form-control" id="myHapName">
                </div>
                <div class="form-group">
                    <label>Last Name</label>
                    <input type="text" class="form-control" id="myHapLName">
                </div>
                <div class="form-group">
                    <label>Email</label>
                    <input type="text" class="form-control" id="myHapEmail">
                    <input type="hidden" class="form-control" id="myHappyResult">
                </div>
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <p class="privacy-text">We respect your privacy and will never share your email address</p>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="btnSubmitOntra" ><i class="fa fa-paper-plane"></i> Send my score</button>

      </div>
    </div>

  </div>
</div>
'.$user_values.'
<script>
var mlr_admin_ajax = "'.admin_url( 'admin-ajax.php' ).'";
</script>
<style>
.myspacer20 {
    clear:both;
    height: 20px;
    width: 100%;
}
</style>
    ';

    return $html_;
}
add_shortcode( 'happy-calc', 'HappyCalculator' );



add_action( 'wp_ajax_sendOntraport', 'sendOntraport' );
add_action( 'wp_ajax_nopriv_sendOntraport', 'sendOntraport' );
function sendOntraport() {
    global $wpdb, $table_prefix;

    // echo ;
    $name = $_POST['hc_name'];
    $lname = $_POST['hc_lname'];
    $email = $_POST['hc_email'];

    $result = $_POST['hc_result'];

    $impFamily = $_POST['impFamily'];
    $impFriends = $_POST['impFriends'];
    $impFinance = $_POST['impFinance'];
    $impHealth = $_POST['impHealth'];
    $impCareer = $_POST['impCareer'];
    $impPersonal = $_POST['impPersonal'];
    $impContribution = $_POST['impContribution'];
    $werFamily = $_POST['werFamily'];
    $werFriends = $_POST['werFriends'];
    $werFinance = $_POST['werFinance'];
    $werHealth = $_POST['werHealth'];
    $werCareer = $_POST['werCareer'];
    $werPersonal = $_POST['werPersonal'];
    $werContribution = $_POST['werContribution'];

    $uid = $_POST['hc_uid'];

    // if(empty($uid)){
        //create user
        $exists = email_exists( $email );
        if ( $exists ) {
            //echo 'Email already exists!';
			// do nothing
        } else {
			// if(empty($name)){
                // $mem_LoginName = $email;
            // }

            // ADD NEW USER TO WP USER LIST
			$website = 'http://domain.com';
            $newpassword = wp_generate_password();
            $userdata = array(
                'user_login'  =>   $email,
                'user_url'    =>  $website,
                'user_pass'   =>  $newpassword,
                'first_name' => $name,
                'last_name' => $lname,
                'role' => 'subscriber',
                'user_email' =>  $email,
            );

            $user_id = wp_insert_user( $userdata ) ;

            //On success
            if ( ! is_wp_error( $user_id ) ) {
                // echo "User created : ". $user_id;
                wp_new_user_notification($user_id, '', 'both');
            }
        }
    // }

    // if(!empty($uid)){
      // insert to database
      // `hid` int(10) NOT NULL auto_increment,
      // `user_id` text NOT NULL,
      // `email` text NOT NULL,
      // `family1` text NOT NULL,
      // `family2` text NOT NULL,
      // `friends1` text NOT NULL,
      // `friends2` text NOT NULL,
      // `finance1` text NOT NULL,
      // `finance2` text NOT NULL,
      // `health1` text NOT NULL,
      // `health2` text NOT NULL,
      // `career1` text NOT NULL,
      // `career2` text NOT NULL,
      // `growth1` text NOT NULL,
      // `growth2` text NOT NULL,
      // `spirit1` text NOT NULL,
      // `spirit2` text NOT NULL,
      // `dateadded` DATETIME,
      $wpdb->insert(
      	$table_prefix.'happiness_score',
      	array(
			'user_id' => $uid,
			'email' => $email,
			'family1' => $impFamily,
			'family2' => $werFamily,
			'friends1' => $impFriends,
			'friends2' => $werFriends,
			'finance1' => $impFinance,
			'finance2' => $werFinance,
			'health1' => $impHealth,
			'health2' => $werHealth,
			'career1' => $impCareer,
			'career2' => $werCareer,
			'growth1' => $impPersonal,
			'growth2' => $werPersonal,
			'spirit1' => $impContribution,
			'spirit2' => $werContribution,
			'totalresult' => $result,
			'dateadded' => date('Y-m-d H:i:s')
      	),
      	array(
			'%s',
			'%s',
			'%s',
			'%s',
			'%s',
			'%s',
			'%s',
			'%s',
			'%s',
			'%s',
			'%s',
			'%s',
			'%s',
			'%s',
			'%s',
			'%s',
			'%s',
			'%s'
      	)
      );
    // }

    require_once(dirname(__FILE__).'/api/ontraport/src/Ontraport.php');

    $client = new OntraportAPI\Ontraport("2_186951_syS8ONZQ1","5rIZ6pYXoKMRELz");
    $requestParams = array(
        "firstname" => "".$name,
        "lastname"  => "".$lname,
        "email"   => $email,
        //"f1557" => $result,  // Happiness Score
        "f1586" => $result,   // Happiness Score Web
        "f1607" => $werPersonal, //"Whe-Personal Growth",
        "f1605" => $werHealth, // "Whe-Health",
        "f1603" => $werFriends, //"Whe-Friends",
        "f1595" => $impFamily, // "Imp-Family",
        "f1596" => $impFriends, // "Imp-Friends",
        "f1597" => $impFinance, // "Imp-Finance",
        "f1598" => $impHealth, //"Imp-Health",
        "f1599" => $impCareer, // "Imp-Career",
        "f1600" => $impPersonal, // "Imp-Personal Growth",
        "f1601" => $impContribution, // "Imp-Spirituality/Contribution",
        "f1608" => $werContribution, // "Whe-Spirituality/Contribution",
        "f1606" => $werCareer, // "Whe-Career",
        "f1604" => $werFinance, // "Whe-Finance",
        "f1602" => $werFamily // "Whe-Family",
    );
    // $response = $client->contact()->create($requestParams);

    // $requestParams = array(
    //     "email"   => $email,
    //     "f1557" => $result,  // Happiness Score
    //     "f1586" => $result   // Happiness Score Web
    // );

    $response = $client->contact()->saveOrUpdate($requestParams);
    print_r($response);

    //user posted variables
    //php mailer variables
    // $subject = "Your Happiness Score Result";
    // $headers = 'From: info@thehappinessalgorithm.com'."\r\n" .
    // 'Reply-To: info@thehappinessalgorithm.com'."\r\n";

    // $message = "Hi ".$name.",<br>
    //     Your Happiness Score is : <strong> ".$result." </strong>
    // ";
    // //Here put your Validation and send mail
    // $sent = wp_mail($email, $subject, $message, $headers);
    // print_r($response);
    // $whatever = intval( $_POST['whatever'] );
    // $whatever += 10;
    //     echo $whatever;
    wp_die();
}

function HappyCalculator_MyAccount($atts){
    $a = shortcode_atts( array(
        'membership' => '',
        'bar' => 'something else',
    ), $atts );


    wp_register_script('happiness-calc-js2', HC_PLUGINPATH.'js/happy-script-logged.js');
    wp_register_script('happiness-calc-js3', HC_PLUGINPATH.'lib/sweetalert.min.js');
    wp_register_script('happiness-calc-js4', HC_PLUGINPATH.'lib/jquery.growl/javascripts/jquery.growl.js');
    wp_register_script('happiness-calc-js5', HC_PLUGINPATH.'lib/RGraph/libraries/RGraph.common.core.js');
    wp_register_script('happiness-calc-js6', HC_PLUGINPATH.'lib/RGraph/libraries/RGraph.common.dynamic.js');
    wp_register_script('happiness-calc-js7', HC_PLUGINPATH.'lib/RGraph/libraries/RGraph.common.tooltips.js');
    wp_register_script('happiness-calc-js8', HC_PLUGINPATH.'lib/RGraph/libraries/RGraph.rose.js');
    // wp_register_script('campdraft_js2', HC_PLUGINPATH.'lib/datatables/jquery.dataTables.min.js');
    // wp_register_script('campdraft_js3', HC_PLUGINPATH.'lib/bootstrap-datepicker/js/bootstrap-datepicker.js');
    // wp_register_script('campdraft_js4', HC_PLUGINPATH.'js/script.js');


    wp_enqueue_script( 'happiness-calc-js1', HC_PLUGINPATH.'lib/bootstrap/js/bootstrap.min.js', array(), '1.0.0', true );
    wp_enqueue_script( 'happiness-calc-js2', HC_PLUGINPATH.'js/happy-script-logged.js', array(), '1.0.0', true );
    wp_enqueue_script( 'happiness-calc-js3', HC_PLUGINPATH.'lib/sweetalert.min.js', array(), '1.0.0', true );
    wp_enqueue_script( 'happiness-calc-js4', HC_PLUGINPATH.'lib/jquery.growl/javascripts/jquery.growl.js', array(), '1.0.0', true );
    wp_enqueue_script( 'happiness-calc-js5', HC_PLUGINPATH.'lib/jRGraph/libraries/RGraph.common.core.js', array(), '1.0.0', true );
    wp_enqueue_script( 'happiness-calc-js6', HC_PLUGINPATH.'lib/jRGraph/libraries/RGraph.common.dynamic.js', array(), '1.0.0', true );
    wp_enqueue_script( 'happiness-calc-js7', HC_PLUGINPATH.'lib/jRGraph/libraries/RGraph.common.tooltips.js', array(), '1.0.0', true );
    wp_enqueue_script( 'happiness-calc-js8', HC_PLUGINPATH.'lib/RGraph/libraries/RGraph.rose.js', array(), '1.0.0', true );
    // wp_enqueue_script( 'campdraft_js2', HC_PLUGINPATH.'lib/datatables/jquery.dataTables.min.js', array(), '1.0.0', true );
    // wp_enqueue_script( 'campdraft_js3', HC_PLUGINPATH.'lib/bootstrap-datepicker/js/bootstrap-datepicker.js', array(), '0.0.1', true );
    // wp_enqueue_script( 'jquery-ui-datepicker' );
    // wp_enqueue_script( 'campdraft_js4', HC_PLUGINPATH.'js/script.js', array(), '0.0.1', true );


    wp_register_style('happiness-calc-st1', HC_PLUGINPATH.'lib/bootstrap/css/bootstrap.min.css');
    wp_register_style('happiness-calc-st2', HC_PLUGINPATH.'lib/font-awesome/css/font-awesome.min.css');
    wp_register_style('happiness-calc-st3', HC_PLUGINPATH.'css/happiness.css');
    wp_register_style('happiness-calc-st4', HC_PLUGINPATH.'lib/jquery.growl/stylesheets/jquery.growl.css');
    // wp_register_style('mlrcd_st3', HC_PLUGINPATH.'lib/datatables/jquery.dataTables.min.css');
    // wp_register_style('mlrcd_st4', HC_PLUGINPATH.'lib/bootstrap-datepicker/css/bootstrap-datepicker.css');

    wp_enqueue_style('happiness-calc-st1');
    wp_enqueue_style('happiness-calc-st2');
    wp_enqueue_style('happiness-calc-st3');
    wp_enqueue_style('happiness-calc-st4');

    ob_start();
    include_once('views/account.views.php');

    return ob_get_clean();
}

add_shortcode( 'happy-calc-account', 'HappyCalculator_MyAccount' );
