<?php
@include("../../../../wp-load.php");

if(!is_user_logged_in()){
    // echo '<script> window.location = \''.home_url().'\'</script>';
    // exit();
    $args = array(
      'echo'           => true,
      'remember'       => true,
      'redirect'       => ( is_ssl() ? 'https://' : 'http://' ) . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'],
      'form_id'        => 'loginform',
      'id_username'    => 'user_login',
      'id_password'    => 'user_pass',
      'id_remember'    => 'rememberme',
      'id_submit'      => 'wp-submit',
      'label_username' => __( 'Username/Email' ),
      'label_password' => __( 'Password' ),
      'label_remember' => __( 'Remember Me' ),
      'label_log_in'   => __( 'Log In' ),
      'value_username' => '',
      'value_remember' => false
    );


    ?>
    <div class="row">
      <div class="center-block" style="width: 320px">
      <?php wp_login_form($args); ?>
      </div>
    </div>
    <?php
}else{
?>
<div class="row">
	<div class="container">
		<?php
		    $current_user = wp_get_current_user();
		    /**
		     * @example Safe usage:
		     * $current_user = wp_get_current_user();
		     * if ( ! $current_user->exists() ) {
		     *     return;
		     * }
		     */
		    echo '<strong>Username: </strong>' . $current_user->user_login . '<br />';
		    // echo 'User email: ' . $current_user->user_email . '<br />';
		    // echo 'User first name: ' . $current_user->user_firstname . '<br />';
		    // echo 'User last name: ' . $current_user->user_lastname . '<br />';
		    // echo 'User display name: ' . $current_user->display_name . '<br />';
		    // echo 'User ID: ' . $current_user->ID . '<br />';
		?>

		<div class="row">
			<div class="col-md-12">

				<canvas id="cvs" width="800" height="300" style="float: right; ">
				    [No canvas support]
				</canvas>

				<div style="clear: both; width: 100%; height: 50px;"></div>
				<h4>Happiness Score</h4>
				<table class="table table-striped">
					<thead>
						<tr>
							<td ><strong>Date</strong></td>
							<td colspan="2"><strong>Family</strong></td>
							<td colspan="2"><strong>Friends</strong></td>
							<td colspan="2"><strong>Finance</strong></td>
							<td colspan="2"><strong>Health</strong></td>
							<td colspan="2"><strong>Career</strong></td>
							<td colspan="2"><strong>Personal Growth</strong></td>
							<td colspan="2"><strong>Spirituality/Contribution</strong></td>
							<td ><strong>Total</strong></td>
						</tr>
					</thead>
					<tbody>

						<?php
						  global $wpdb, $table_prefix;

						  $sql_score = "select * from `".$table_prefix."happiness_score` where `user_id` = '".$current_user->ID."' or `email` = '".$current_user->user_email."' order by `dateadded` desc";
						  $rs_score  = $wpdb->get_results($sql_score);
						  if(count($rs_score) > 0){
							  //$data = array();
							  foreach($rs_score as $score){
								//echo '<option value="'.$zones->abbrv.'">'.$zones->zone.' - ['.$zones->abbrv.']</option>';
								//[family1] => 1 [family2] => 10 [friends1] => 2 [friends2] => 10 [finance1] => 3 [finance2] => 10 [health1] => 4 [health2] => 10 [career1] => 5 [career2] => 10 [growth1] => 6 [growth2] => 10 [spirit1] => 7 [spirit2] => 10 [totalresult] => 10.0
								$data[] = '['.$score->family2 .'],['. 
										$score->friends2 .'],['. 
										$score->finance2 .'],['. 
										$score->health2 .'],['. 
										$score->career2 .'],['. 
										$score->growth2 .'],['. 
										$score->spirit2 .']';
								echo '
									<tr>
										<td >'.date('M. j, Y', strtotime($score->dateadded)).'</td>
										
										<td >'.$score->family1.'</td>
										<td >'.$score->family2.'</td>

										<td >'.$score->friends1.'</td>
										<td >'.$score->friends2.'</td>

										<td >'.$score->finance1.'</td>
										<td >'.$score->finance2.'</td>

										<td >'.$score->health1.'</td>
										<td >'.$score->health2.'</td>

										<td >'.$score->career1.'</td>
										<td >'.$score->career2.'</td>

										<td >'.$score->growth1.'</td>
										<td >'.$score->growth2.'</td>

										<td >'.$score->spirit1.'</td>
										<td >'.$score->spirit2.'</td>

										<td >'.$score->totalresult.'</td>
									</tr>
								';
							  }
							  //print_r($rs_score);							  
						  }else{
							echo '
						<tr>
							<td >0000-00-00</td>
							
							<td >0</td>
							<td >0</td>

							<td >0</td>
							<td >0</td>

							<td >0</td>
							<td >0</td>

							<td >0</td>
							<td >0</td>

							<td >0</td>
							<td >0</td>

							<td >0</td>
							<td >0</td>

							<td >0</td>
							<td >0</td>

							<td >0</td>
						</tr>
							';
						  }

						?>
					</tbody>
				</table>
			</div>
		</div>

	</div>
</div>

<script>
jQuery(function(){

    // This is a stacked 3D Rose chart. The data is the same as a regular stacked
    // Rose chart and looks just like the data that you'd pass to a grouped or
    // stacked Bar chart.
    new RGraph.Rose({
        id: 'cvs',
        // data: [[4,8,7],[6,5,8],[4,5,3],[4,9,8],[3,8,6],[4,6,3],[1,5,8]],
		data: [<? echo $data[0]; ?>],
        options: {
            colorsStroke: 'rgba(0,0,0,0)',
            margin: 7,
            variant: 'stacked3d',
            variantThreedDepth: 10,
            labelsAxes: 'n',
            colors: ['gradient(#faa:red)','Gradient(#afa:green)','gradient(#aaf:#ddf)'],
/*             tooltips: [
                'Wilf','Harry','Gerrard',
                'Wilf','Harry','Gerrard',
                'Wilf','Harry','Gerrard',
                'Wilf','Harry','Gerrard',
                'Wilf','Harry','Gerrard',
                'Wilf','Harry','Gerrard',
                'Wilf','Harry','Gerrard'
            ], */
            labels: ['Family','Friends','Finance','Health','Career','Personal Growth','Spirituality/Contribution'],
            textAccessible: false
        }
    }).draw().on('tooltip', function (obj)
    {
        // This is a 'tooltip' event listener. It's triggered whenever a
        // tooltip is shown.

        // Get the tooltip DIV tag from the RGraph registry
        var tooltip = RGraph.Registry.get('tooltip');

        // Add the numerical value, using the sequentialised data and the tooltip index,
        // to the tooltip DIV tag
        tooltip.innerHTML += ': <b>' + obj.data_seq[tooltip.__index__] + '%</b>';
        
        // Adding the extra text above means that the width of the tooltip has to be
        // increased too - so do that here.
        tooltip.style.width = parseInt(tooltip.style.width) + 30 + 'px';
    });

});
</script>
<?php
}
